﻿namespace ApkaLokalna
{
    partial class LoginScreen
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.loginBox = new System.Windows.Forms.TextBox();
            this.hasloBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.logujButton = new System.Windows.Forms.Button();
            this.messageBox = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Login: ";
            // 
            // loginBox
            // 
            this.loginBox.Location = new System.Drawing.Point(174, 55);
            this.loginBox.MaxLength = 10;
            this.loginBox.Name = "loginBox";
            this.loginBox.Size = new System.Drawing.Size(127, 20);
            this.loginBox.TabIndex = 1;
            // 
            // hasloBox
            // 
            this.hasloBox.Location = new System.Drawing.Point(174, 94);
            this.hasloBox.MaxLength = 10;
            this.hasloBox.Name = "hasloBox";
            this.hasloBox.Size = new System.Drawing.Size(127, 20);
            this.hasloBox.TabIndex = 3;
            this.hasloBox.UseSystemPasswordChar = true;
            this.hasloBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Hasło: ";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // logujButton
            // 
            this.logujButton.Location = new System.Drawing.Point(174, 142);
            this.logujButton.Name = "logujButton";
            this.logujButton.Size = new System.Drawing.Size(75, 23);
            this.logujButton.TabIndex = 4;
            this.logujButton.Text = "ZALOGUJ";
            this.logujButton.UseVisualStyleBackColor = true;
            this.logujButton.Click += new System.EventHandler(this.logujButton_Click);
            // 
            // messageBox
            // 
            this.messageBox.AutoSize = true;
            this.messageBox.Location = new System.Drawing.Point(171, 184);
            this.messageBox.Name = "messageBox";
            this.messageBox.Size = new System.Drawing.Size(0, 13);
            this.messageBox.TabIndex = 5;
            // 
            // LoginScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 249);
            this.Controls.Add(this.messageBox);
            this.Controls.Add(this.logujButton);
            this.Controls.Add(this.hasloBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.loginBox);
            this.Controls.Add(this.label1);
            this.Name = "LoginScreen";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox loginBox;
        private System.Windows.Forms.TextBox hasloBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button logujButton;
        private System.Windows.Forms.Label messageBox;
    }
}

