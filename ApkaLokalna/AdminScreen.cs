﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApkaLokalna
{

    public partial class AdminScreen : Form
    {

        private ekstEntities context;

        public AdminScreen()
        {
            InitializeComponent();
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void loadContext()
        {
            context = new ekstEntities();
            context.Drużyny.Load();
            context.Gole.Load();
            context.Kartki.Load();
            context.Miasta.Load();
            context.Piłkarze.Load();
            context.Sędziowie.Load();
            context.Spotkania.Load();
            context.Trenerzy.Load();
            context.Users.Load();
        }

        private void bindSources()
        {
            this.drużynyBindingSource.DataSource = context.Drużyny.Local.ToBindingList();
            bindingNavigator1.BindingSource = drużynyBindingSource;
            this.goleBindingSource.DataSource = context.Gole.Local.ToBindingList();
            bindingNavigator2.BindingSource = goleBindingSource;
            this.kartkiBindingSource.DataSource = context.Kartki.Local.ToBindingList();
            bindingNavigator3.BindingSource = kartkiBindingSource;
            this.miastaBindingSource.DataSource = context.Miasta.Local.ToBindingList();
            bindingNavigator4.BindingSource = miastaBindingSource;
            this.piłkarzeBindingSource.DataSource = context.Piłkarze.Local.ToBindingList();
            bindingNavigator5.BindingSource = piłkarzeBindingSource;
            this.sędziowieBindingSource.DataSource = context.Sędziowie.Local.ToBindingList();
            bindingNavigator6.BindingSource = sędziowieBindingSource;
            this.spotkaniaBindingSource.DataSource = context.Spotkania.Local.ToBindingList();
            bindingNavigator7.BindingSource = spotkaniaBindingSource;
            this.trenerzyBindingSource.DataSource = context.Trenerzy.Local.ToBindingList();
            bindingNavigator8.BindingSource = trenerzyBindingSource;
            this.usersBindingSource.DataSource = context.Users.Local.ToBindingList();
            bindingNavigator9.BindingSource = usersBindingSource;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            loadContext();
            bindSources();
        }


        private void bindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.context.SaveChanges();
                this.dataGridView1.Refresh();
            }catch(Exception exe)
            {
                textBox1.Text = exe.Message;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            this.context.Dispose();
        }

        private void bindingNavigatorSaveItem1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.context.SaveChanges();
                this.dataGridView2.Refresh();
            }
            catch (Exception exe)
            {
                textBox2.Text = exe.Message;

            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.context.SaveChanges();
                this.dataGridView3.Refresh();
            }
            catch (Exception exe)
            {
                textBox3.Text = exe.Message;
            }
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.context.SaveChanges();
                this.dataGridView4.Refresh();
            }
            catch (Exception exe)
            {
                textBox4.Text = exe.Message;
            }
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.context.SaveChanges();
                this.dataGridView5.Refresh();
            }
            catch (Exception exe)
            {
                textBox5.Text = exe.Message;
            }
        }

        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.context.SaveChanges();
                this.dataGridView6.Refresh();
            }
            catch (Exception exe)
            {
                textBox6.Text = exe.Message;
            }
        }

        private void toolStripButton15_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.context.SaveChanges();
                this.dataGridView7.Refresh();
            }
            catch (Exception exe)
            {
                textBox7.Text = exe.Message;
            }
        }

        private void toolStripButton18_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.context.SaveChanges();
                this.dataGridView8.Refresh();
            }
            catch (Exception exe)
            {
                textBox8.Text = exe.Message;
            }
        }

        private void toolStripButton21_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.context.SaveChanges();
                this.dataGridView9.Refresh();
            }
            catch (Exception exe)
            {
                textBox9.Text = exe.Message;
            }
        }
    }
}
