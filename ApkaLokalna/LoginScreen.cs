﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApkaLokalna
{

    public partial class LoginScreen : Form
    {
        public static String conString = "Data Source=z710\bazajarek;Initial Catalog=ekst;Integrated Security=True";
        private ObservableCollection<Users> users { get; set; }
        private ekstEntities _cntx { get; set; }

        public LoginScreen()
        {
            InitializeComponent();
            users = new ObservableCollection<Users>();
            load();
        }


        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void load()
        {
            _cntx = new ekstEntities();
            _cntx.Users.Load();
            users = _cntx.Users.Local;
        }

        public LoginScreen giveThisObject()
        {
            return this;
        }


        private void logujButton_Click(object sender, EventArgs e)
        {
            if( users.Any( p => p.login.Trim() == loginBox.Text.Trim() && p.haslo.Trim() == hasloBox.Text.Trim() ))
            {
                Users user1 = users.Single( p => p.login.Trim() == loginBox.Text.Trim() && p.haslo.Trim() == hasloBox.Text.Trim());

                if ( user1.rola.Value == 1)
                {
                    (new AdminScreen()).ShowDialog();
                }
                else if ( user1.rola.Value == 2 ) {
                    (new UserScreen()).ShowDialog();
                }
                else if( user1.rola.Value == 3 )
                {
                    (new PzpnScreen()).ShowDialog();
                }


            }
            else
            {
                messageBox.Text = "Błąd logowania";
            } 

        }
    }
}
