//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ApkaLokalna
{
    using System;
    using System.Collections.Generic;
    
    public partial class Piłkarze
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Piłkarze()
        {
            this.Gole = new HashSet<Gole>();
            this.Kartki = new HashSet<Kartki>();
        }
    
        public int p_id { get; set; }
        public Nullable<int> d_id { get; set; }
        public string p_imie { get; set; }
        public string p_nazwisko { get; set; }
        public Nullable<int> p_wiek { get; set; }
        public string p_pozycja { get; set; }
    
        public virtual Drużyny Drużyny { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Gole> Gole { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kartki> Kartki { get; set; }
    }
}
