﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApkaLokalna
{
    public partial class UserScreen : Form
    {
    
        public UserScreen()
        {
            InitializeComponent();
            initializeGrid();
        }

        private void initializeGrid()
        {
            SqlConnection sqlcon = new SqlConnection(@"Data Source=z710\bazajarek;Initial Catalog=ekst;Integrated Security=True");

            try
            {
                string query = "SELECT p.p_nazwisko as Nazwisko, count(p.p_id) AS Strzelone FROM Gole g " +
                    "INNER JOIN Piłkarze p ON p.p_id = g.p_id GROUP BY p.p_nazwisko ORDER BY Strzelone desc";
                SqlCommand cmd = new SqlCommand(query, sqlcon);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                strzelcyDataGrid.DataSource = dt.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                string query = "SELECT m.m_nazwa as Miasto, d1.d_nazwa as Gospodarz, d2.d_nazwa as Gość," +
                    " se.se_nazwisko as Sędzia, sp_data as Data FROM Spotkania s " +
                    "INNER JOIN Drużyny d1 ON s.d_id = d1.d_id " +
                    "INNER JOIN Drużyny d2 ON s.Dru_d_id = d2.d_id " +
                    "INNER JOIN Miasta m ON s.m_id = m.m_id " +
                    "INNER JOIN Sędziowie se ON s.se_id = se.se_id ";
                SqlCommand cmd = new SqlCommand(query, sqlcon);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                kalendarzDataGrid.DataSource = dt.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                string query = "SELECT d.d_nazwa as Nazwa, t.t_imie + ' ' + t.t_nazwisko as Trener," +
                    " m.m_nazwa as Miasto, d.d_punkty as Punkty FROM Drużyny d INNER JOIN Trenerzy t ON d.t_id = t.t_id " +
                    "INNER JOIN Miasta m ON d.m_id = m.m_id order by d.d_punkty desc";
                SqlCommand cmd = new SqlCommand(query, sqlcon);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                druzynyDataGrid.DataSource = dt.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
