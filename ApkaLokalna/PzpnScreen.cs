﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApkaLokalna
{
    public partial class PzpnScreen : Form
    {

        private ekstEntities cntx;
        public PzpnScreen()
        {

            InitializeComponent();
            cntx = new ekstEntities();
            cntx.Sędziowie.Load();
            comboBox1.DataSource = cntx.Sędziowie.ToList();
            comboBox1.DisplayMember = "se_nazwisko";
            comboBox1.ValueMember = "se_id";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection sqlcon = new SqlConnection(@"Data Source=z710\bazajarek;Initial Catalog=ekst;Integrated Security=True");

            int id = (int)comboBox1.SelectedValue;

            try
            {
                string query = "SELECT count(s.se_id) FROM Spotkania s INNER JOIN Sędziowie se ON s.se_id = se.se_id WHERE se.se_id = " + id + " GROUP BY se.se_nazwisko";
                SqlCommand cmd = new SqlCommand(query, sqlcon);
                cmd.Connection.Open();
                if( cmd.ExecuteScalar() == null)
                {
                    textBox1.Text = "0";
                }
                else
                {
                    textBox1.Text = cmd.ExecuteScalar().ToString();
                }
                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                string query = "SELECT count(k.sp_id) FROM Spotkania s INNER JOIN Sędziowie se ON s.se_id = se.se_id " +
                    "INNER JOIN Kartki k ON s.sp_id = k.sp_id WHERE se.se_id = " + id + " GROUP BY se.se_nazwisko";
                SqlCommand cmd = new SqlCommand(query, sqlcon);
                cmd.Connection.Open();
                if( cmd.ExecuteScalar() == null)
                {
                    textBox2.Text = "0";
                }
                else
                {
                    textBox2.Text = cmd.ExecuteScalar().ToString();
                }
                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
