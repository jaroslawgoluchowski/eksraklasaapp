﻿namespace ApkaLokalna
{
    partial class UserScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.returnButton = new System.Windows.Forms.Button();
            this.strzelcyDataGrid = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.druzynyDataGrid = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.kalendarzDataGrid = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.strzelcyDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.druzynyDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kalendarzDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // returnButton
            // 
            this.returnButton.Location = new System.Drawing.Point(785, 12);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(75, 23);
            this.returnButton.TabIndex = 0;
            this.returnButton.Text = "WRÓĆ";
            this.returnButton.UseVisualStyleBackColor = true;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // strzelcyDataGrid
            // 
            this.strzelcyDataGrid.AllowUserToAddRows = false;
            this.strzelcyDataGrid.AllowUserToDeleteRows = false;
            this.strzelcyDataGrid.AllowUserToResizeColumns = false;
            this.strzelcyDataGrid.AllowUserToResizeRows = false;
            this.strzelcyDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.strzelcyDataGrid.Location = new System.Drawing.Point(12, 51);
            this.strzelcyDataGrid.Name = "strzelcyDataGrid";
            this.strzelcyDataGrid.ReadOnly = true;
            this.strzelcyDataGrid.Size = new System.Drawing.Size(267, 121);
            this.strzelcyDataGrid.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ranking króla strzelców";
            // 
            // druzynyDataGrid
            // 
            this.druzynyDataGrid.AllowUserToAddRows = false;
            this.druzynyDataGrid.AllowUserToDeleteRows = false;
            this.druzynyDataGrid.AllowUserToResizeColumns = false;
            this.druzynyDataGrid.AllowUserToResizeRows = false;
            this.druzynyDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.druzynyDataGrid.Location = new System.Drawing.Point(342, 51);
            this.druzynyDataGrid.Name = "druzynyDataGrid";
            this.druzynyDataGrid.ReadOnly = true;
            this.druzynyDataGrid.Size = new System.Drawing.Size(518, 150);
            this.druzynyDataGrid.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(339, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tabela ";
            // 
            // kalendarzDataGrid
            // 
            this.kalendarzDataGrid.AllowUserToAddRows = false;
            this.kalendarzDataGrid.AllowUserToDeleteRows = false;
            this.kalendarzDataGrid.AllowUserToResizeColumns = false;
            this.kalendarzDataGrid.AllowUserToResizeRows = false;
            this.kalendarzDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.kalendarzDataGrid.Location = new System.Drawing.Point(12, 232);
            this.kalendarzDataGrid.Name = "kalendarzDataGrid";
            this.kalendarzDataGrid.ReadOnly = true;
            this.kalendarzDataGrid.Size = new System.Drawing.Size(582, 156);
            this.kalendarzDataGrid.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 216);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Kalendarz spotkań";
            // 
            // UserScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 400);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.druzynyDataGrid);
            this.Controls.Add(this.kalendarzDataGrid);
            this.Controls.Add(this.strzelcyDataGrid);
            this.Controls.Add(this.returnButton);
            this.Name = "UserScreen";
            this.Text = "UserScreen";
            ((System.ComponentModel.ISupportInitialize)(this.strzelcyDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.druzynyDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kalendarzDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.DataGridView strzelcyDataGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView druzynyDataGrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView kalendarzDataGrid;
        private System.Windows.Forms.Label label3;
    }
}