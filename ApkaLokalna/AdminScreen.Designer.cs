﻿namespace ApkaLokalna
{
    partial class AdminScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminScreen));
            this.returnButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bindingNavigator2 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSaveItem1 = new System.Windows.Forms.ToolStripButton();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.bindingNavigator3 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.min = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.bindingNavigator4 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.bindingNavigator5 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.dataGridView9 = new System.Windows.Forms.DataGridView();
            this.bindingNavigator6 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigator7 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton15 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigator8 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton16 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton17 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton18 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigator9 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton19 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton20 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton21 = new System.Windows.Forms.ToolStripButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.didDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.midDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dnazwaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dpunktyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.drużynyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.didDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minutaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kartkiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.midDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mnazwaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.miastaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pidDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.didDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pimieDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnazwiskoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pwiekDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ppozycjaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.piłkarzeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.seidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seimieDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.senazwiskoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sędziowieBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spidDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.midDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.didDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.drudidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seidDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spdataDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spotkaniaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tidDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.didDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timieDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tnazwiskoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trenerzyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iduzytDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hasloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rolaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).BeginInit();
            this.bindingNavigator2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator3)).BeginInit();
            this.bindingNavigator3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator4)).BeginInit();
            this.bindingNavigator4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator5)).BeginInit();
            this.bindingNavigator5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator6)).BeginInit();
            this.bindingNavigator6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator7)).BeginInit();
            this.bindingNavigator7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator8)).BeginInit();
            this.bindingNavigator8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator9)).BeginInit();
            this.bindingNavigator9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.drużynyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.goleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kartkiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.miastaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.piłkarzeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sędziowieBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spotkaniaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trenerzyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // returnButton
            // 
            this.returnButton.Location = new System.Drawing.Point(713, 12);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(75, 23);
            this.returnButton.TabIndex = 1;
            this.returnButton.Text = "WRÓĆ";
            this.returnButton.UseVisualStyleBackColor = true;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(695, 417);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.bindingNavigator1);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(687, 391);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Drużyny";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigator1.CountItem = null;
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.bindingNavigatorSaveItem});
            this.bindingNavigator1.Location = new System.Drawing.Point(3, 3);
            this.bindingNavigator1.MoveFirstItem = null;
            this.bindingNavigator1.MoveLastItem = null;
            this.bindingNavigator1.MoveNextItem = null;
            this.bindingNavigator1.MovePreviousItem = null;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = null;
            this.bindingNavigator1.Size = new System.Drawing.Size(681, 25);
            this.bindingNavigator1.TabIndex = 1;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Dodaj nowy";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Usuń";
            // 
            // bindingNavigatorSaveItem
            // 
            this.bindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorSaveItem.Image")));
            this.bindingNavigatorSaveItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorSaveItem.Name = "bindingNavigatorSaveItem";
            this.bindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorSaveItem.Text = "Zapisz";
            this.bindingNavigatorSaveItem.Click += new System.EventHandler(this.bindingNavigatorSaveItem_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.didDataGridViewTextBoxColumn,
            this.tidDataGridViewTextBoxColumn,
            this.midDataGridViewTextBoxColumn,
            this.dnazwaDataGridViewTextBoxColumn,
            this.dpunktyDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.drużynyBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 31);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(553, 270);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.bindingNavigator2);
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(687, 391);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Gole";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // bindingNavigator2
            // 
            this.bindingNavigator2.AddNewItem = this.bindingNavigatorAddNewItem1;
            this.bindingNavigator2.CountItem = null;
            this.bindingNavigator2.DeleteItem = this.bindingNavigatorDeleteItem1;
            this.bindingNavigator2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorAddNewItem1,
            this.bindingNavigatorDeleteItem1,
            this.bindingNavigatorSaveItem1});
            this.bindingNavigator2.Location = new System.Drawing.Point(3, 3);
            this.bindingNavigator2.MoveFirstItem = null;
            this.bindingNavigator2.MoveLastItem = null;
            this.bindingNavigator2.MoveNextItem = null;
            this.bindingNavigator2.MovePreviousItem = null;
            this.bindingNavigator2.Name = "bindingNavigator2";
            this.bindingNavigator2.PositionItem = null;
            this.bindingNavigator2.Size = new System.Drawing.Size(681, 25);
            this.bindingNavigator2.TabIndex = 1;
            this.bindingNavigator2.Text = "bindingNavigator2";
            // 
            // bindingNavigatorAddNewItem1
            // 
            this.bindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem1.Image")));
            this.bindingNavigatorAddNewItem1.Name = "bindingNavigatorAddNewItem1";
            this.bindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem1.Text = "Dodaj nowy";
            // 
            // bindingNavigatorDeleteItem1
            // 
            this.bindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem1.Image")));
            this.bindingNavigatorDeleteItem1.Name = "bindingNavigatorDeleteItem1";
            this.bindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem1.Text = "Usuń";
            // 
            // bindingNavigatorSaveItem1
            // 
            this.bindingNavigatorSaveItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorSaveItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorSaveItem1.Image")));
            this.bindingNavigatorSaveItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bindingNavigatorSaveItem1.Name = "bindingNavigatorSaveItem1";
            this.bindingNavigatorSaveItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorSaveItem1.Text = "toolStripButton1";
            this.bindingNavigatorSaveItem1.Click += new System.EventHandler(this.bindingNavigatorSaveItem1_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.didDataGridViewTextBoxColumn1,
            this.pidDataGridViewTextBoxColumn,
            this.spidDataGridViewTextBoxColumn,
            this.minutaDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.goleBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(0, 45);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(687, 259);
            this.dataGridView2.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textBox3);
            this.tabPage3.Controls.Add(this.bindingNavigator3);
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(687, 391);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Kartki";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.textBox4);
            this.tabPage4.Controls.Add(this.bindingNavigator4);
            this.tabPage4.Controls.Add(this.dataGridView4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(687, 391);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Miasta";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.textBox5);
            this.tabPage5.Controls.Add(this.bindingNavigator5);
            this.tabPage5.Controls.Add(this.dataGridView5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(687, 391);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Piłkarze";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.textBox6);
            this.tabPage6.Controls.Add(this.bindingNavigator6);
            this.tabPage6.Controls.Add(this.dataGridView6);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(687, 391);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Sędziowie";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.textBox7);
            this.tabPage7.Controls.Add(this.bindingNavigator7);
            this.tabPage7.Controls.Add(this.dataGridView7);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(687, 391);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Spotkania";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.textBox8);
            this.tabPage8.Controls.Add(this.bindingNavigator8);
            this.tabPage8.Controls.Add(this.dataGridView8);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(687, 391);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Trenerzy";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.textBox9);
            this.tabPage9.Controls.Add(this.bindingNavigator9);
            this.tabPage9.Controls.Add(this.dataGridView9);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(687, 391);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Users";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn2,
            this.min});
            this.dataGridView3.DataSource = this.kartkiBindingSource;
            this.dataGridView3.Location = new System.Drawing.Point(3, 46);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(513, 258);
            this.dataGridView3.TabIndex = 1;
            // 
            // bindingNavigator3
            // 
            this.bindingNavigator3.AddNewItem = this.toolStripButton1;
            this.bindingNavigator3.CountItem = null;
            this.bindingNavigator3.DeleteItem = this.toolStripButton2;
            this.bindingNavigator3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3});
            this.bindingNavigator3.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator3.MoveFirstItem = null;
            this.bindingNavigator3.MoveLastItem = null;
            this.bindingNavigator3.MoveNextItem = null;
            this.bindingNavigator3.MovePreviousItem = null;
            this.bindingNavigator3.Name = "bindingNavigator3";
            this.bindingNavigator3.PositionItem = null;
            this.bindingNavigator3.Size = new System.Drawing.Size(687, 25);
            this.bindingNavigator3.TabIndex = 2;
            this.bindingNavigator3.Text = "bindingNavigator3";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Dodaj nowy";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.RightToLeftAutoMirrorImage = true;
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Usuń";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton1";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // min
            // 
            this.min.DataPropertyName = "min";
            this.min.HeaderText = "Minuta";
            this.min.Name = "min";
            // 
            // dataGridView4
            // 
            this.dataGridView4.AutoGenerateColumns = false;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.midDataGridViewTextBoxColumn1,
            this.mnazwaDataGridViewTextBoxColumn});
            this.dataGridView4.DataSource = this.miastaBindingSource;
            this.dataGridView4.Location = new System.Drawing.Point(0, 28);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(333, 276);
            this.dataGridView4.TabIndex = 0;
            // 
            // bindingNavigator4
            // 
            this.bindingNavigator4.AddNewItem = this.toolStripButton4;
            this.bindingNavigator4.CountItem = null;
            this.bindingNavigator4.DeleteItem = this.toolStripButton5;
            this.bindingNavigator4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton6});
            this.bindingNavigator4.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator4.MoveFirstItem = null;
            this.bindingNavigator4.MoveLastItem = null;
            this.bindingNavigator4.MoveNextItem = null;
            this.bindingNavigator4.MovePreviousItem = null;
            this.bindingNavigator4.Name = "bindingNavigator4";
            this.bindingNavigator4.PositionItem = null;
            this.bindingNavigator4.Size = new System.Drawing.Size(687, 25);
            this.bindingNavigator4.TabIndex = 3;
            this.bindingNavigator4.Text = "bindingNavigator4";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.RightToLeftAutoMirrorImage = true;
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "Dodaj nowy";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "Usuń";
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "toolStripButton1";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // dataGridView5
            // 
            this.dataGridView5.AutoGenerateColumns = false;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pidDataGridViewTextBoxColumn1,
            this.didDataGridViewTextBoxColumn2,
            this.pimieDataGridViewTextBoxColumn,
            this.pnazwiskoDataGridViewTextBoxColumn,
            this.pwiekDataGridViewTextBoxColumn,
            this.ppozycjaDataGridViewTextBoxColumn});
            this.dataGridView5.DataSource = this.piłkarzeBindingSource;
            this.dataGridView5.Location = new System.Drawing.Point(3, 28);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(681, 302);
            this.dataGridView5.TabIndex = 1;
            // 
            // bindingNavigator5
            // 
            this.bindingNavigator5.AddNewItem = this.toolStripButton7;
            this.bindingNavigator5.CountItem = null;
            this.bindingNavigator5.DeleteItem = this.toolStripButton8;
            this.bindingNavigator5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton9});
            this.bindingNavigator5.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator5.MoveFirstItem = null;
            this.bindingNavigator5.MoveLastItem = null;
            this.bindingNavigator5.MoveNextItem = null;
            this.bindingNavigator5.MovePreviousItem = null;
            this.bindingNavigator5.Name = "bindingNavigator5";
            this.bindingNavigator5.PositionItem = null;
            this.bindingNavigator5.Size = new System.Drawing.Size(687, 25);
            this.bindingNavigator5.TabIndex = 4;
            this.bindingNavigator5.Text = "bindingNavigator5";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.RightToLeftAutoMirrorImage = true;
            this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton7.Text = "Dodaj nowy";
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.RightToLeftAutoMirrorImage = true;
            this.toolStripButton8.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton8.Text = "Usuń";
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton9.Text = "toolStripButton1";
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // dataGridView6
            // 
            this.dataGridView6.AutoGenerateColumns = false;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.seidDataGridViewTextBoxColumn,
            this.seimieDataGridViewTextBoxColumn,
            this.senazwiskoDataGridViewTextBoxColumn});
            this.dataGridView6.DataSource = this.sędziowieBindingSource;
            this.dataGridView6.Location = new System.Drawing.Point(23, 35);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(623, 268);
            this.dataGridView6.TabIndex = 2;
            // 
            // dataGridView7
            // 
            this.dataGridView7.AutoGenerateColumns = false;
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.spidDataGridViewTextBoxColumn1,
            this.midDataGridViewTextBoxColumn2,
            this.didDataGridViewTextBoxColumn3,
            this.drudidDataGridViewTextBoxColumn,
            this.seidDataGridViewTextBoxColumn1,
            this.spdataDataGridViewTextBoxColumn});
            this.dataGridView7.DataSource = this.spotkaniaBindingSource;
            this.dataGridView7.Location = new System.Drawing.Point(0, 28);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.Size = new System.Drawing.Size(687, 304);
            this.dataGridView7.TabIndex = 2;
            // 
            // dataGridView8
            // 
            this.dataGridView8.AutoGenerateColumns = false;
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tidDataGridViewTextBoxColumn1,
            this.didDataGridViewTextBoxColumn4,
            this.timieDataGridViewTextBoxColumn,
            this.tnazwiskoDataGridViewTextBoxColumn});
            this.dataGridView8.DataSource = this.trenerzyBindingSource;
            this.dataGridView8.Location = new System.Drawing.Point(0, 28);
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.Size = new System.Drawing.Size(491, 280);
            this.dataGridView8.TabIndex = 2;
            // 
            // dataGridView9
            // 
            this.dataGridView9.AutoGenerateColumns = false;
            this.dataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView9.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iduzytDataGridViewTextBoxColumn,
            this.loginDataGridViewTextBoxColumn,
            this.hasloDataGridViewTextBoxColumn,
            this.rolaDataGridViewTextBoxColumn});
            this.dataGridView9.DataSource = this.usersBindingSource;
            this.dataGridView9.Location = new System.Drawing.Point(0, 28);
            this.dataGridView9.Name = "dataGridView9";
            this.dataGridView9.Size = new System.Drawing.Size(445, 213);
            this.dataGridView9.TabIndex = 2;
            // 
            // bindingNavigator6
            // 
            this.bindingNavigator6.AddNewItem = this.toolStripButton10;
            this.bindingNavigator6.CountItem = null;
            this.bindingNavigator6.DeleteItem = this.toolStripButton11;
            this.bindingNavigator6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton10,
            this.toolStripButton11,
            this.toolStripButton12});
            this.bindingNavigator6.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator6.MoveFirstItem = null;
            this.bindingNavigator6.MoveLastItem = null;
            this.bindingNavigator6.MoveNextItem = null;
            this.bindingNavigator6.MovePreviousItem = null;
            this.bindingNavigator6.Name = "bindingNavigator6";
            this.bindingNavigator6.PositionItem = null;
            this.bindingNavigator6.Size = new System.Drawing.Size(687, 25);
            this.bindingNavigator6.TabIndex = 5;
            this.bindingNavigator6.Text = "bindingNavigator6";
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.RightToLeftAutoMirrorImage = true;
            this.toolStripButton10.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton10.Text = "Dodaj nowy";
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.RightToLeftAutoMirrorImage = true;
            this.toolStripButton11.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton11.Text = "Usuń";
            // 
            // toolStripButton12
            // 
            this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton12.Image")));
            this.toolStripButton12.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton12.Name = "toolStripButton12";
            this.toolStripButton12.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton12.Text = "toolStripButton1";
            this.toolStripButton12.Click += new System.EventHandler(this.toolStripButton12_Click);
            // 
            // bindingNavigator7
            // 
            this.bindingNavigator7.AddNewItem = this.toolStripButton13;
            this.bindingNavigator7.CountItem = null;
            this.bindingNavigator7.DeleteItem = this.toolStripButton14;
            this.bindingNavigator7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton13,
            this.toolStripButton14,
            this.toolStripButton15});
            this.bindingNavigator7.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator7.MoveFirstItem = null;
            this.bindingNavigator7.MoveLastItem = null;
            this.bindingNavigator7.MoveNextItem = null;
            this.bindingNavigator7.MovePreviousItem = null;
            this.bindingNavigator7.Name = "bindingNavigator7";
            this.bindingNavigator7.PositionItem = null;
            this.bindingNavigator7.Size = new System.Drawing.Size(687, 25);
            this.bindingNavigator7.TabIndex = 5;
            this.bindingNavigator7.Text = "bindingNavigator7";
            // 
            // toolStripButton13
            // 
            this.toolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton13.Image")));
            this.toolStripButton13.Name = "toolStripButton13";
            this.toolStripButton13.RightToLeftAutoMirrorImage = true;
            this.toolStripButton13.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton13.Text = "Dodaj nowy";
            // 
            // toolStripButton14
            // 
            this.toolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton14.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton14.Image")));
            this.toolStripButton14.Name = "toolStripButton14";
            this.toolStripButton14.RightToLeftAutoMirrorImage = true;
            this.toolStripButton14.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton14.Text = "Usuń";
            // 
            // toolStripButton15
            // 
            this.toolStripButton15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton15.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton15.Image")));
            this.toolStripButton15.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton15.Name = "toolStripButton15";
            this.toolStripButton15.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton15.Text = "toolStripButton1";
            this.toolStripButton15.Click += new System.EventHandler(this.toolStripButton15_Click);
            // 
            // bindingNavigator8
            // 
            this.bindingNavigator8.AddNewItem = this.toolStripButton16;
            this.bindingNavigator8.CountItem = null;
            this.bindingNavigator8.DeleteItem = this.toolStripButton17;
            this.bindingNavigator8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton16,
            this.toolStripButton17,
            this.toolStripButton18});
            this.bindingNavigator8.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator8.MoveFirstItem = null;
            this.bindingNavigator8.MoveLastItem = null;
            this.bindingNavigator8.MoveNextItem = null;
            this.bindingNavigator8.MovePreviousItem = null;
            this.bindingNavigator8.Name = "bindingNavigator8";
            this.bindingNavigator8.PositionItem = null;
            this.bindingNavigator8.Size = new System.Drawing.Size(687, 25);
            this.bindingNavigator8.TabIndex = 5;
            this.bindingNavigator8.Text = "bindingNavigator8";
            // 
            // toolStripButton16
            // 
            this.toolStripButton16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton16.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton16.Image")));
            this.toolStripButton16.Name = "toolStripButton16";
            this.toolStripButton16.RightToLeftAutoMirrorImage = true;
            this.toolStripButton16.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton16.Text = "Dodaj nowy";
            // 
            // toolStripButton17
            // 
            this.toolStripButton17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton17.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton17.Image")));
            this.toolStripButton17.Name = "toolStripButton17";
            this.toolStripButton17.RightToLeftAutoMirrorImage = true;
            this.toolStripButton17.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton17.Text = "Usuń";
            // 
            // toolStripButton18
            // 
            this.toolStripButton18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton18.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton18.Image")));
            this.toolStripButton18.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton18.Name = "toolStripButton18";
            this.toolStripButton18.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton18.Text = "toolStripButton1";
            this.toolStripButton18.Click += new System.EventHandler(this.toolStripButton18_Click);
            // 
            // bindingNavigator9
            // 
            this.bindingNavigator9.AddNewItem = this.toolStripButton19;
            this.bindingNavigator9.CountItem = null;
            this.bindingNavigator9.DeleteItem = this.toolStripButton20;
            this.bindingNavigator9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton19,
            this.toolStripButton20,
            this.toolStripButton21});
            this.bindingNavigator9.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator9.MoveFirstItem = null;
            this.bindingNavigator9.MoveLastItem = null;
            this.bindingNavigator9.MoveNextItem = null;
            this.bindingNavigator9.MovePreviousItem = null;
            this.bindingNavigator9.Name = "bindingNavigator9";
            this.bindingNavigator9.PositionItem = null;
            this.bindingNavigator9.Size = new System.Drawing.Size(687, 25);
            this.bindingNavigator9.TabIndex = 5;
            this.bindingNavigator9.Text = "bindingNavigator9";
            // 
            // toolStripButton19
            // 
            this.toolStripButton19.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton19.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton19.Image")));
            this.toolStripButton19.Name = "toolStripButton19";
            this.toolStripButton19.RightToLeftAutoMirrorImage = true;
            this.toolStripButton19.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton19.Text = "Dodaj nowy";
            // 
            // toolStripButton20
            // 
            this.toolStripButton20.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton20.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton20.Image")));
            this.toolStripButton20.Name = "toolStripButton20";
            this.toolStripButton20.RightToLeftAutoMirrorImage = true;
            this.toolStripButton20.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton20.Text = "Usuń";
            // 
            // toolStripButton21
            // 
            this.toolStripButton21.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton21.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton21.Image")));
            this.toolStripButton21.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton21.Name = "toolStripButton21";
            this.toolStripButton21.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton21.Text = "toolStripButton1";
            this.toolStripButton21.Click += new System.EventHandler(this.toolStripButton21_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(0, 307);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(687, 81);
            this.textBox1.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(0, 310);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(687, 81);
            this.textBox2.TabIndex = 8;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(0, 310);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(687, 81);
            this.textBox3.TabIndex = 8;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(0, 310);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(687, 81);
            this.textBox4.TabIndex = 8;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(0, 336);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(687, 55);
            this.textBox5.TabIndex = 8;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(0, 309);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(687, 81);
            this.textBox6.TabIndex = 8;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(0, 338);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(687, 53);
            this.textBox7.TabIndex = 8;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(0, 314);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(687, 81);
            this.textBox8.TabIndex = 8;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(0, 310);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(687, 81);
            this.textBox9.TabIndex = 8;
            // 
            // didDataGridViewTextBoxColumn
            // 
            this.didDataGridViewTextBoxColumn.DataPropertyName = "d_id";
            this.didDataGridViewTextBoxColumn.HeaderText = "ID Drużyny";
            this.didDataGridViewTextBoxColumn.Name = "didDataGridViewTextBoxColumn";
            // 
            // tidDataGridViewTextBoxColumn
            // 
            this.tidDataGridViewTextBoxColumn.DataPropertyName = "t_id";
            this.tidDataGridViewTextBoxColumn.HeaderText = "ID Trenera";
            this.tidDataGridViewTextBoxColumn.Name = "tidDataGridViewTextBoxColumn";
            // 
            // midDataGridViewTextBoxColumn
            // 
            this.midDataGridViewTextBoxColumn.DataPropertyName = "m_id";
            this.midDataGridViewTextBoxColumn.HeaderText = "ID Miasta";
            this.midDataGridViewTextBoxColumn.Name = "midDataGridViewTextBoxColumn";
            // 
            // dnazwaDataGridViewTextBoxColumn
            // 
            this.dnazwaDataGridViewTextBoxColumn.DataPropertyName = "d_nazwa";
            this.dnazwaDataGridViewTextBoxColumn.HeaderText = "Nazwa";
            this.dnazwaDataGridViewTextBoxColumn.Name = "dnazwaDataGridViewTextBoxColumn";
            // 
            // dpunktyDataGridViewTextBoxColumn
            // 
            this.dpunktyDataGridViewTextBoxColumn.DataPropertyName = "d_punkty";
            this.dpunktyDataGridViewTextBoxColumn.HeaderText = "Punkty";
            this.dpunktyDataGridViewTextBoxColumn.Name = "dpunktyDataGridViewTextBoxColumn";
            // 
            // drużynyBindingSource
            // 
            this.drużynyBindingSource.DataSource = typeof(ApkaLokalna.Drużyny);
            // 
            // didDataGridViewTextBoxColumn1
            // 
            this.didDataGridViewTextBoxColumn1.DataPropertyName = "d_id";
            this.didDataGridViewTextBoxColumn1.HeaderText = "ID Drużyny";
            this.didDataGridViewTextBoxColumn1.Name = "didDataGridViewTextBoxColumn1";
            // 
            // pidDataGridViewTextBoxColumn
            // 
            this.pidDataGridViewTextBoxColumn.DataPropertyName = "p_id";
            this.pidDataGridViewTextBoxColumn.HeaderText = "ID Piłkarza";
            this.pidDataGridViewTextBoxColumn.Name = "pidDataGridViewTextBoxColumn";
            // 
            // spidDataGridViewTextBoxColumn
            // 
            this.spidDataGridViewTextBoxColumn.DataPropertyName = "sp_id";
            this.spidDataGridViewTextBoxColumn.HeaderText = "ID Spotkania";
            this.spidDataGridViewTextBoxColumn.Name = "spidDataGridViewTextBoxColumn";
            // 
            // minutaDataGridViewTextBoxColumn
            // 
            this.minutaDataGridViewTextBoxColumn.DataPropertyName = "minuta";
            this.minutaDataGridViewTextBoxColumn.HeaderText = "Minuta";
            this.minutaDataGridViewTextBoxColumn.Name = "minutaDataGridViewTextBoxColumn";
            // 
            // goleBindingSource
            // 
            this.goleBindingSource.DataSource = typeof(ApkaLokalna.Gole);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "d_id";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID Drużyny";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "sp_id";
            this.dataGridViewTextBoxColumn3.HeaderText = "ID Spotkania";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "p_id";
            this.dataGridViewTextBoxColumn2.HeaderText = "ID Piłkarza";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // kartkiBindingSource
            // 
            this.kartkiBindingSource.DataSource = typeof(ApkaLokalna.Kartki);
            // 
            // midDataGridViewTextBoxColumn1
            // 
            this.midDataGridViewTextBoxColumn1.DataPropertyName = "m_id";
            this.midDataGridViewTextBoxColumn1.HeaderText = "ID Miasta";
            this.midDataGridViewTextBoxColumn1.Name = "midDataGridViewTextBoxColumn1";
            // 
            // mnazwaDataGridViewTextBoxColumn
            // 
            this.mnazwaDataGridViewTextBoxColumn.DataPropertyName = "m_nazwa";
            this.mnazwaDataGridViewTextBoxColumn.HeaderText = "Nazwa Miasta";
            this.mnazwaDataGridViewTextBoxColumn.Name = "mnazwaDataGridViewTextBoxColumn";
            // 
            // miastaBindingSource
            // 
            this.miastaBindingSource.DataSource = typeof(ApkaLokalna.Miasta);
            // 
            // pidDataGridViewTextBoxColumn1
            // 
            this.pidDataGridViewTextBoxColumn1.DataPropertyName = "p_id";
            this.pidDataGridViewTextBoxColumn1.HeaderText = "ID Piłkarza";
            this.pidDataGridViewTextBoxColumn1.Name = "pidDataGridViewTextBoxColumn1";
            // 
            // didDataGridViewTextBoxColumn2
            // 
            this.didDataGridViewTextBoxColumn2.DataPropertyName = "d_id";
            this.didDataGridViewTextBoxColumn2.HeaderText = "ID Drużyny";
            this.didDataGridViewTextBoxColumn2.Name = "didDataGridViewTextBoxColumn2";
            // 
            // pimieDataGridViewTextBoxColumn
            // 
            this.pimieDataGridViewTextBoxColumn.DataPropertyName = "p_imie";
            this.pimieDataGridViewTextBoxColumn.HeaderText = "Imię";
            this.pimieDataGridViewTextBoxColumn.Name = "pimieDataGridViewTextBoxColumn";
            // 
            // pnazwiskoDataGridViewTextBoxColumn
            // 
            this.pnazwiskoDataGridViewTextBoxColumn.DataPropertyName = "p_nazwisko";
            this.pnazwiskoDataGridViewTextBoxColumn.HeaderText = "Nazwisko";
            this.pnazwiskoDataGridViewTextBoxColumn.Name = "pnazwiskoDataGridViewTextBoxColumn";
            // 
            // pwiekDataGridViewTextBoxColumn
            // 
            this.pwiekDataGridViewTextBoxColumn.DataPropertyName = "p_wiek";
            this.pwiekDataGridViewTextBoxColumn.HeaderText = "Wiek";
            this.pwiekDataGridViewTextBoxColumn.Name = "pwiekDataGridViewTextBoxColumn";
            // 
            // ppozycjaDataGridViewTextBoxColumn
            // 
            this.ppozycjaDataGridViewTextBoxColumn.DataPropertyName = "p_pozycja";
            this.ppozycjaDataGridViewTextBoxColumn.HeaderText = "Pozycja";
            this.ppozycjaDataGridViewTextBoxColumn.Name = "ppozycjaDataGridViewTextBoxColumn";
            // 
            // piłkarzeBindingSource
            // 
            this.piłkarzeBindingSource.DataSource = typeof(ApkaLokalna.Piłkarze);
            // 
            // seidDataGridViewTextBoxColumn
            // 
            this.seidDataGridViewTextBoxColumn.DataPropertyName = "se_id";
            this.seidDataGridViewTextBoxColumn.HeaderText = "ID Sędziego";
            this.seidDataGridViewTextBoxColumn.Name = "seidDataGridViewTextBoxColumn";
            // 
            // seimieDataGridViewTextBoxColumn
            // 
            this.seimieDataGridViewTextBoxColumn.DataPropertyName = "se_imie";
            this.seimieDataGridViewTextBoxColumn.HeaderText = "Imie";
            this.seimieDataGridViewTextBoxColumn.Name = "seimieDataGridViewTextBoxColumn";
            // 
            // senazwiskoDataGridViewTextBoxColumn
            // 
            this.senazwiskoDataGridViewTextBoxColumn.DataPropertyName = "se_nazwisko";
            this.senazwiskoDataGridViewTextBoxColumn.HeaderText = "Nazwisko";
            this.senazwiskoDataGridViewTextBoxColumn.Name = "senazwiskoDataGridViewTextBoxColumn";
            // 
            // sędziowieBindingSource
            // 
            this.sędziowieBindingSource.DataSource = typeof(ApkaLokalna.Sędziowie);
            // 
            // spidDataGridViewTextBoxColumn1
            // 
            this.spidDataGridViewTextBoxColumn1.DataPropertyName = "sp_id";
            this.spidDataGridViewTextBoxColumn1.HeaderText = "ID Spotkania";
            this.spidDataGridViewTextBoxColumn1.Name = "spidDataGridViewTextBoxColumn1";
            // 
            // midDataGridViewTextBoxColumn2
            // 
            this.midDataGridViewTextBoxColumn2.DataPropertyName = "m_id";
            this.midDataGridViewTextBoxColumn2.HeaderText = "ID Miasta";
            this.midDataGridViewTextBoxColumn2.Name = "midDataGridViewTextBoxColumn2";
            // 
            // didDataGridViewTextBoxColumn3
            // 
            this.didDataGridViewTextBoxColumn3.DataPropertyName = "d_id";
            this.didDataGridViewTextBoxColumn3.HeaderText = "ID Gospodarza";
            this.didDataGridViewTextBoxColumn3.Name = "didDataGridViewTextBoxColumn3";
            // 
            // drudidDataGridViewTextBoxColumn
            // 
            this.drudidDataGridViewTextBoxColumn.DataPropertyName = "Dru_d_id";
            this.drudidDataGridViewTextBoxColumn.HeaderText = "ID Gościa";
            this.drudidDataGridViewTextBoxColumn.Name = "drudidDataGridViewTextBoxColumn";
            // 
            // seidDataGridViewTextBoxColumn1
            // 
            this.seidDataGridViewTextBoxColumn1.DataPropertyName = "se_id";
            this.seidDataGridViewTextBoxColumn1.HeaderText = "ID Sędziego";
            this.seidDataGridViewTextBoxColumn1.Name = "seidDataGridViewTextBoxColumn1";
            // 
            // spdataDataGridViewTextBoxColumn
            // 
            this.spdataDataGridViewTextBoxColumn.DataPropertyName = "sp_data";
            this.spdataDataGridViewTextBoxColumn.HeaderText = "Data";
            this.spdataDataGridViewTextBoxColumn.Name = "spdataDataGridViewTextBoxColumn";
            // 
            // spotkaniaBindingSource
            // 
            this.spotkaniaBindingSource.DataSource = typeof(ApkaLokalna.Spotkania);
            // 
            // tidDataGridViewTextBoxColumn1
            // 
            this.tidDataGridViewTextBoxColumn1.DataPropertyName = "t_id";
            this.tidDataGridViewTextBoxColumn1.HeaderText = "ID Trenera";
            this.tidDataGridViewTextBoxColumn1.Name = "tidDataGridViewTextBoxColumn1";
            // 
            // didDataGridViewTextBoxColumn4
            // 
            this.didDataGridViewTextBoxColumn4.DataPropertyName = "d_id";
            this.didDataGridViewTextBoxColumn4.HeaderText = "ID Drużyny";
            this.didDataGridViewTextBoxColumn4.Name = "didDataGridViewTextBoxColumn4";
            // 
            // timieDataGridViewTextBoxColumn
            // 
            this.timieDataGridViewTextBoxColumn.DataPropertyName = "t_imie";
            this.timieDataGridViewTextBoxColumn.HeaderText = "Imie";
            this.timieDataGridViewTextBoxColumn.Name = "timieDataGridViewTextBoxColumn";
            // 
            // tnazwiskoDataGridViewTextBoxColumn
            // 
            this.tnazwiskoDataGridViewTextBoxColumn.DataPropertyName = "t_nazwisko";
            this.tnazwiskoDataGridViewTextBoxColumn.HeaderText = "Nazwisko";
            this.tnazwiskoDataGridViewTextBoxColumn.Name = "tnazwiskoDataGridViewTextBoxColumn";
            // 
            // trenerzyBindingSource
            // 
            this.trenerzyBindingSource.DataSource = typeof(ApkaLokalna.Trenerzy);
            // 
            // iduzytDataGridViewTextBoxColumn
            // 
            this.iduzytDataGridViewTextBoxColumn.DataPropertyName = "id_uzyt";
            this.iduzytDataGridViewTextBoxColumn.HeaderText = "ID Użytkownika";
            this.iduzytDataGridViewTextBoxColumn.Name = "iduzytDataGridViewTextBoxColumn";
            // 
            // loginDataGridViewTextBoxColumn
            // 
            this.loginDataGridViewTextBoxColumn.DataPropertyName = "login";
            this.loginDataGridViewTextBoxColumn.HeaderText = "Login";
            this.loginDataGridViewTextBoxColumn.Name = "loginDataGridViewTextBoxColumn";
            // 
            // hasloDataGridViewTextBoxColumn
            // 
            this.hasloDataGridViewTextBoxColumn.DataPropertyName = "haslo";
            this.hasloDataGridViewTextBoxColumn.HeaderText = "Hasło";
            this.hasloDataGridViewTextBoxColumn.Name = "hasloDataGridViewTextBoxColumn";
            // 
            // rolaDataGridViewTextBoxColumn
            // 
            this.rolaDataGridViewTextBoxColumn.DataPropertyName = "rola";
            this.rolaDataGridViewTextBoxColumn.HeaderText = "Rola";
            this.rolaDataGridViewTextBoxColumn.Name = "rolaDataGridViewTextBoxColumn";
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataSource = typeof(ApkaLokalna.Users);
            // 
            // AdminScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.returnButton);
            this.Name = "AdminScreen";
            this.Text = "AdminScreen";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator2)).EndInit();
            this.bindingNavigator2.ResumeLayout(false);
            this.bindingNavigator2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator3)).EndInit();
            this.bindingNavigator3.ResumeLayout(false);
            this.bindingNavigator3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator4)).EndInit();
            this.bindingNavigator4.ResumeLayout(false);
            this.bindingNavigator4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator5)).EndInit();
            this.bindingNavigator5.ResumeLayout(false);
            this.bindingNavigator5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator6)).EndInit();
            this.bindingNavigator6.ResumeLayout(false);
            this.bindingNavigator6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator7)).EndInit();
            this.bindingNavigator7.ResumeLayout(false);
            this.bindingNavigator7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator8)).EndInit();
            this.bindingNavigator8.ResumeLayout(false);
            this.bindingNavigator8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator9)).EndInit();
            this.bindingNavigator9.ResumeLayout(false);
            this.bindingNavigator9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.drużynyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.goleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kartkiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.miastaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.piłkarzeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sędziowieBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spotkaniaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trenerzyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource drużynyBindingSource;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorSaveItem;
        private System.Windows.Forms.BindingNavigator bindingNavigator2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorSaveItem1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource goleBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn didDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn midDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dnazwaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dpunktyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn didDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn pidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn spidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minutaDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingNavigator bindingNavigator3;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.BindingSource kartkiBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn min;
        private System.Windows.Forms.BindingNavigator bindingNavigator4;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridViewTextBoxColumn midDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn mnazwaDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource miastaBindingSource;
        private System.Windows.Forms.BindingNavigator bindingNavigator5;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.DataGridViewTextBoxColumn pidDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn didDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn pimieDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pnazwiskoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pwiekDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ppozycjaDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource piłkarzeBindingSource;
        private System.Windows.Forms.BindingNavigator bindingNavigator6;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.DataGridViewTextBoxColumn seidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seimieDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn senazwiskoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource sędziowieBindingSource;
        private System.Windows.Forms.BindingNavigator bindingNavigator7;
        private System.Windows.Forms.ToolStripButton toolStripButton13;
        private System.Windows.Forms.ToolStripButton toolStripButton14;
        private System.Windows.Forms.ToolStripButton toolStripButton15;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.DataGridViewTextBoxColumn spidDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn midDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn didDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn drudidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seidDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn spdataDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource spotkaniaBindingSource;
        private System.Windows.Forms.BindingNavigator bindingNavigator8;
        private System.Windows.Forms.ToolStripButton toolStripButton16;
        private System.Windows.Forms.ToolStripButton toolStripButton17;
        private System.Windows.Forms.ToolStripButton toolStripButton18;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.DataGridViewTextBoxColumn tidDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn didDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn timieDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tnazwiskoDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource trenerzyBindingSource;
        private System.Windows.Forms.BindingNavigator bindingNavigator9;
        private System.Windows.Forms.ToolStripButton toolStripButton19;
        private System.Windows.Forms.ToolStripButton toolStripButton20;
        private System.Windows.Forms.ToolStripButton toolStripButton21;
        private System.Windows.Forms.DataGridView dataGridView9;
        private System.Windows.Forms.DataGridViewTextBoxColumn iduzytDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn loginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hasloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rolaDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource usersBindingSource;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
    }
}